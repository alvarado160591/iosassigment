//
//  String+Extension.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import Foundation

extension String {
    var removeLeadingZeroes: String {
        return replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
    }
}
