//
//  UITableViewCell+Extension.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import UIKit

extension UITableViewCell {
    @objc class var identifier: String {
        return String(describing: self)
    }
}
