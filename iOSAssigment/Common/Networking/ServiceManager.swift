//
//  ServiceManager.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import Alamofire

struct PropertyListKey {
    static let PropertyList = "plist"
    static let URL = "URL"
}

enum Endpoint: String {
    case getUsers
}

enum StatusCode: Int {
    case successful = 200
    case successfulPost = 201
    case noContent = 204
    case unauthorized = 401
    case internalServer = 500
}

class ServicesManager: NSObject {
    
    fileprivate var endpointsDictionary: Dictionary<String, AnyObject> = [:]
    
    let endpointKey = "Endpoints"
    static let shared = ServicesManager()
    
    override private init() {
        super.init()
        self.setEndpointsDictionary()
    }
    
    func urlForEndpoint(endpoint: Endpoint) -> String {
        guard let service = endpointsDictionary[endpoint.rawValue], let api = service[PropertyListKey.URL], let serviceApi = api as? String else {
            return String()
        }
        return serviceApi
    }
}

func genericRequest<T: Decodable>(urlString: String,
                                  method: HTTPMethod,
                                  headers: HTTPHeaders? = nil,
                                  parameters: Parameters? = nil,
                                  encoding: ParameterEncoding? = URLEncoding.httpBody,
                                  showLoading: Bool? = true,
                                  completion: @escaping (_ error: Error?, _ object: T?, _ statusCode: Int?) -> Void) {
    if showLoading ?? true { LoadingActivity.start() }
    let requestEncoding = encoding ?? URLEncoding.httpBody
    Alamofire.request(urlString, method: method, parameters: parameters, encoding: requestEncoding, headers: headers).response { response in
        LoadingActivity.stop()
        guard response.error == nil else {
            completion(response.error, nil, nil)
            return
        }
        if response.response?.statusCode == StatusCode.successful.rawValue || response.response?.statusCode == StatusCode.successfulPost.rawValue {
            guard let data = response.data else {
                completion(nil, nil, response.response?.statusCode)
                return
            }
            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                completion(nil, model, response.response?.statusCode)
            } catch let jsonError {
                completion(jsonError, nil, response.response?.statusCode)
            }
        } else {
            completion(nil, nil, response.response?.statusCode)
        }
    }
}

private extension ServicesManager {
    func setEndpointsDictionary() {
        if let path = Bundle.main.path(forResource: endpointKey, ofType: PropertyListKey.PropertyList) {
            if let dictionary = NSDictionary(contentsOfFile: path) {
                if let dic = dictionary[endpointKey] as? Dictionary<String, AnyObject> {
                    endpointsDictionary = dic
                }
            }
        }
    }
}
