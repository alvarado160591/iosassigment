//
//  User.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import Foundation

protocol UserProtocol {
    var name: String? { get }
    var image: String? { get }
    var items: [String]? { get }
}

struct User: Codable, UserProtocol {
    let name: String?
    let image: String?
    var items: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
        case items = "items"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try? values.decodeIfPresent(String.self, forKey: .name)
        image = try? values.decodeIfPresent(String.self, forKey: .image)
        items = try? values.decodeIfPresent([String].self, forKey: .items)
    }
    
    init(name: String, image: String, items: [String]) {
        self.name = name
        self.image = image
        self.items = items
    }
}
