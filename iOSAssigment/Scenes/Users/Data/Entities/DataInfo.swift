//
//  Data.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import Foundation

protocol DataProtocol {
    var users: [User]? { get }
}

struct DataInfo: Codable, DataProtocol {
    let users: [User]?
    
    enum CodingKeys: String, CodingKey {
        case users = "users"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        users = try? values.decodeIfPresent([User].self, forKey: .users)
    }
    
    init(users: [User]) {
        self.users = users
    }
}
