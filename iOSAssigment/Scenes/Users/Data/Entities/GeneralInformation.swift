//
//  GeneralInformation.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import Foundation

protocol GeneralInformationProtocol {
    var status: Bool? { get }
    var message: String? { get }
    var data: DataInfo? { get }
}

struct GeneralInformation: Codable, GeneralInformationProtocol {
    let status: Bool?
    let message: String?
    let data: DataInfo?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try? values.decodeIfPresent(Bool.self, forKey: .status)
        message = try? values.decodeIfPresent(String.self, forKey: .message)
        data = try? values.decodeIfPresent(DataInfo.self, forKey: .data)
    }
    
    init(status: Bool, message: String, data: DataInfo) {
        self.status = status
        self.message = message
        self.data = data
    }
}
