//
//  UsersTableViewController.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import UIKit
import Foundation

class UsersTableViewController: UITableViewController {
    
    var users: [User]? = [User]()
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        registerCells()
        fetchData()
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: UserCell.identifier, bundle: nil), forCellReuseIdentifier: UserCell.identifier)
    }
    
    private func fetchData() {
        var url = ServicesManager.shared.urlForEndpoint(endpoint: .getUsers)
        url = url.replacingOccurrences(of: "{offset}", with: "10")
        url = url.replacingOccurrences(of: "{limit}", with: "10")
        let endpoint = String(format: url, "\(url.removeLeadingZeroes)")
            DispatchQueue.main.async {
                genericRequest(urlString: endpoint, method: .get) { (error: Error?, generalInformation: GeneralInformation?, _) in
                    if error == nil {
                        guard let users = generalInformation?.data?.users else { return }
                        self.users = users
                        self.tableView.reloadData()
                    }
                }
            }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.identifier, for: indexPath) as? UserCell else { return UITableViewCell() }
        cell.setUserImage(path: self.users?[indexPath.row].image)
        cell.userName.text = self.users?[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.user = self.users?[indexPath.row]
        self.performSegue(withIdentifier: "toUserDetails", sender: self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toUserDetails" {
            let controller = segue.destination as! UserDetailTableViewController
            controller.user = self.user
        }
    }
}
