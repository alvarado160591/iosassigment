//
//  TwoImagesCell.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import UIKit

class TwoImagesCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImages(path: String?, path2: String?) {
        guard let url1 = path else { return }
        guard let url2 = path2 else { return }
        leftImageView.imageFromUrl(urlString: url1)
        rightImageView.imageFromUrl(urlString: url2)
    }
}
