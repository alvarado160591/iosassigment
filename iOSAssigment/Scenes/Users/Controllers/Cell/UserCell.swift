//
//  UserCell.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImage.makeRoundedImageView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.userImage?.image = nil
        self.userName?.text = ""
    }
    
    func setUserImage(path: String?) {
        guard let url = path else { return }
        userImage.imageFromUrl(urlString: url)
    }
}
