//
//  UserDetailTableViewController.swift
//  iOSAssigment
//
//  Created by Hector Jose Alvarado Chapa on 04/12/19.
//  Copyright © 2019 Hector Jose Alvarado Chapa. All rights reserved.
//

import UIKit

public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

class UserDetailTableViewController: UITableViewController {

    var user: User?
    var isOdd: Bool = false
    var oddImageUrl: String = String()
    var leftItems: [String] = [String]()
    var rightItems: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        setItems()
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: UserCell.identifier, bundle: nil), forCellReuseIdentifier: UserCell.identifier)
        tableView.register(UINib(nibName: OneImageCell.identifier, bundle: nil), forCellReuseIdentifier: OneImageCell.identifier)
        tableView.register(UINib(nibName: TwoImagesCell.identifier, bundle: nil), forCellReuseIdentifier: TwoImagesCell.identifier)
    }
    
    private func setItems() {
        guard let itemsCount = user?.items?.count else { return }
        if itemsCount % 2 != 0 {
            self.oddImageUrl = user?.items?[0] ?? String()
            self.user?.items?.removeFirst()
        }
        self.setItemsToArray()
        self.tableView.reloadData()
    }
    
    private func setItemsToArray() {
        guard let items = self.user?.items else { return }
        var isLeft: Bool = true
        for item in items {
            if isLeft {
                self.leftItems.append(item)
            } else {
                self.rightItems.append(item)
            }
            isLeft = (isLeft) ? false : true
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let rows = (self.leftItems.count == self.rightItems.count) ? self.leftItems.count : 0
        return (oddImageUrl.isEmpty) ? rows : rows + 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 && !self.oddImageUrl.isEmpty {
            return screenWidth
        }
        return screenWidth / 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 && !self.oddImageUrl.isEmpty {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OneImageCell.identifier, for: indexPath) as? OneImageCell else { return UITableViewCell() }
            cell.setImage(path: self.oddImageUrl)
            return cell
        } else {
            let index = self.oddImageUrl.isEmpty ? indexPath.row : indexPath.row - 1
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TwoImagesCell.identifier, for: indexPath) as? TwoImagesCell else { return UITableViewCell() }
            cell.setImages(path: self.leftItems[index], path2: self.rightItems[index])
            return cell
        }
    }
}
